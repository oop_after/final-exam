/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.thanawuth.findarea;

import java.io.Serializable;

/**
 *
 * @author Windows 10
 */
public class FindArea implements Serializable{
    private double TriangleBase;
    private double TriangleHeight;
    private double SquareSide;
    private double Wide;
    private double Long;
    private double Radius;

    public FindArea(double TriangleBase, double TriangleHeight, double SquareSide,double Wide,double Long, double Radius ) {
        this.TriangleBase = TriangleBase;
        this.TriangleHeight = TriangleHeight;
        this.SquareSide = SquareSide;
        this.Wide = Wide;
        this.Long = Long;
        this.Radius = Radius;
                
    }
    public double getTriangleBase() {
        return TriangleBase;
    }

    public void setTriangleBase(double TriangleBase) {
        this.TriangleBase = TriangleBase;
    }
    public double getTriangleHeight() {
        return TriangleHeight;
    }

    public void setTriangleHeight(double TriangleHeight) {
        this.TriangleHeight = TriangleHeight;
    }
    public double getSquareSide() {
        return SquareSide;
    }

    public void setSquareSide(double SquareSide) {
        this.SquareSide = SquareSide;
    }
    public double getWide() {
        return Wide;
    }

    public void setWide(double Wide) {
        this.Wide = Wide;
    }
    public double getLong() {
        return Long;
    }

    public void setLong(double Long) {
        this.Long = Long;
    }
    
    public void setRadius(double Radius) {
        this.Radius = Radius;
    }
    public double getRadius() {
        return Radius;
    }
}
